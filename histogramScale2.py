import os, argparse,glob, array
import signal
import sys

bdebug=True
locationOfRunNumber=1
useAMIForTotalEvents=False
def debug(message):
    if bdebug:
        print message

def signal_handler(signal, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#parser.add_argument("--directory", dest='directory', default="/atlas/uct3/data/users/khildebr/RandomConeV204MCweighted", help="Path to the input directory")
parser.add_argument("--directory", dest='directory', default="/share/t3data3/khildebr/submitDirJESV65weighted", help="Path to the input directory")
parser.add_argument("--runInfoFile", dest='runInfoFile', default="/home/khildebr/AMIRetrieve/XS_inputFiles.txt", help="Path to the txt file containing run info")
parser.add_argument("--runTotalEventsFile", dest='runTotalEventsFile', default="/home/khildebr/AMIRetrieve/EventCountsV65JES.txt", help="Path to the txt file containing run info for total events")
#parser.add_argument("--treeName", dest='treeName', default="outTree", help="Name of trees to be reweighted")

args = parser.parse_args()

#print "hello"
from ROOT import *
#check if runInfoFile exists
if not os.path.isfile(args.runInfoFile):
    print("%s file does not exist"%(args.runInfoFile))
    sys.exit(0)
    
#check if directory exists
if not os.path.isdir(args.directory):
    print("%s directory does not exist"%(args.directory))
    sys.exit(0)
    
#check that there are subdirectories
if os.listdir(args.directory)==[]:
    print("%s directory is empty"%(args.directory))
    sys.exit(0)
    
#now have confirmed directory exists and is not empty
for subdirectory in os.listdir(args.directory):
    fullPathOfSubdirectory="%s/%s"%(args.directory,subdirectory)
    debug("checking dir " + subdirectory)
    #only look at root subdirectories
    if "root" not in subdirectory:
        debug("did not find \'root\' in name")
        continue
    
    if not os.path.isdir(fullPathOfSubdirectory):
        debug("not a directory")
	continue

    #get Run Number
    runNumber=subdirectory.split(".")[locationOfRunNumber]
    debug("runNumber is " + runNumber)
    #open run Info File and get information on run. Run information is stored as (runNumber,XS,FE,# Events)
    inputRunInfoFile = open(args.runInfoFile,'r')
    bool_reachedEndOfFile=True
    XS=0
    FE=0
    Events=0
    for line in inputRunInfoFile:
        if runNumber==line.split(" ")[0]:
            bool_reachedEndOfFile=False
            XS=line.split(" ")[1]
            FE=line.split(" ")[2]
            if useAMIForTotalEvents:
                Events=line.split(" ")[3]
            break
    inputRunInfoFile.close()

    if not useAMIForTotalEvents:
        inputRunInfoFile = open(args.runTotalEventsFile,'r')
        for line in inputRunInfoFile:
            if runNumber==line.split(" ")[0]:
                bool_reachedEndOfFile=False
                Events=line.split(" ")[1]
                break   
        inputRunInfoFile.close()
    if bool_reachedEndOfFile==True:
        print "runNumber " + runNumber +" exists in directory but no runInformation is contained in either " + args.runInfoFile + " or " + args.runTotalEventsFile
        continue

    #print the list of files contained in subdirectory
    #debug(os.listdir("%s/%s"%(args.directory,subdirectory)))
    #debug(fullPathOfSubdirectory)
    for rootFile in os.listdir(fullPathOfSubdirectory):
        if ".root" not in rootFile:
            continue
        debug(rootFile)
        inFile = TFile.Open("%s/%s"%(fullPathOfSubdirectory,rootFile), "UPDATE")
        #Need to loop over all objects in root file twice in order to avoid infinite loop
        #if done in one step, because we are creating objects the listOfKeys would grow indefinitely if done in just one loop. 
        #The way it is written now insures it only runs on original objects
        listOfKeys=[]
        for key in inFile.GetListOfKeys():
            listOfKeys.append(key)
        for key in listOfKeys:
            debug(key)
            class1=gROOT.GetClass(key.GetClassName())
            if class1.InheritsFrom("TH1") or class1.InheritsFrom("TH2") or class1.InheritsFrom("TH3"):
                newHisto2=key.ReadObj().Clone("unweighted_%s"%key.GetName());
                newHisto=key.ReadObj().Clone(key.GetName())
                newHisto.Scale(float(XS)*float(FE)/float(Events))
                newHisto.Write("",TObject.kOverwrite)
                newHisto2.Write()
        inFile.Close()
        
