# ReweightHistograms

### Author: Kevin Hildebrand
### Email: khildebrand@uchicago.edu

This package is for reweighting MC histograms according to the number of events in the dataset, filter efficiency and cross section.

It is assumed that you have already done the command `setupATLAS`.

## Quick overview of scripts contained in this package:

`collectAMIInfo.py`
	This script takes an input file which contains the names of the different datasets and for each dataset gets the cross section, filter efficiency and number of events from AMI.
	The number of events retrieved from AMI should probably not be used, it isn't always correct.
	
`getNumberOfEvents.py`
	This script looks in a directory and scans through each subdirectory that contains the word root in it and adds up the number of events in each root file and outputs the results into a
	text file.
	
`histogramScale2.py`
	This script scales the histograms using the info from the text files produced by the previous scripts. It makes a copy of each histogram with name unweighted added to the beginning of
	the name of each histogram. The weighted histograms are the ones with the original names.
	
## More details on how to run each script:

`collectAMIInfo.py`

This script takes only one input. The input is the name of text file which contains the full name of all datasets you want to retrieve AMI info for.

For example with a text file `inputNames.txt` which contains the following lines:
```
mc15_13TeV.361020.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ0W.merge.DAOD_JETM5.e3569_s2576_s2132_r7267_r6282_p2530
mc15_13TeV.361021.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1W.merge.DAOD_JETM5.e3569_s2576_s2132_r7267_r6282_p2530
```

With `inputNames.txt` and `collectAMIInfo.py` in the same directory, first setup PyAMI:

```
localSetupPyAMI
```
You may need to use your grid certificate so also type the command:

```
voms-proxy-init -voms atlas
```

and enter your password for your grid certificate.

Then run the script:

```
python collectAMIInfo.py --file inputNames.txt
```

The script should then output a text file `XS_inputNames.txt`

Within this text file there will be a line for dataset name given in the input text file. The first column is the run number, second is cross section (in inverse fb), third is filter efficiency, and
fourth is number of events. Again the number of events retrieved from AMI can sometimes be incorrect. This is the reason for the next script.

## getNumberOfEvents.py

This script takes two inputs. The directory the root files exist in and the file name to output the results in. 

There are two different ways that this script can get the number of events. Either from trees or from cutflow histograms. Which it uses is changed in the script by changing the boolean `useCutflow` from
either true or false near the top of the script.

If `useCutflow` is false, then you must give the name of the tree you are counting events from by setting `treeName` near the top of the script.

If `useCutflow` is true, then you must give the name of the cutflow histogram you are counting events from by setting `nameOfHistogram` near the top of the script. You must also set which bin in the cutflow
histogram you will be using for the number events (this should almost always be the first bin) by setting `whichBin`.

The directory structure must have the following. Let us assume you give it the path `/path/to/directory` as the input for the directory you want to get events from. Then there must
be subdirectories within `/path/to/directory` that contain the root files. That is the root files can not be directly inside the directory you pass it as input. The subdirectories must contain the following
two things. The run number of the dataset with a `.` at the begginning and end and the word `root` somewhere in the name.  An example of what the directory should look like is below:

You have `path/to/directory` and inside this are the two subdirectories (and possilbly other files/subdirectories):

some.text.361020.some.other.text_root
some.text.361021.some.other.text_root

then within each of these subdirectories is all the .root files (for example `user.khildebr.9299600._000002.out.root`).

Then you must set one last thing in the script. The number of periods in the subdirectory that proceed the run number (This is done so that the script can extract the run number from the subdirectory name and
use it in the output). So for our example (`some.text.361020.some.other.text_root`) there are 2 periods preceeding 361020 so you would set `locationOfRunNumber=3` near the top of the script.

So now that the script has been set appropriately for running, this is how it is now run:

If you haven't done rcSetup or setup root yet in your current session you may need to do the command `localSetupROOT`.

Then from the directory that `getNumberOfEvents.py` is located in run the command:

```
python getNumberOfEvents.py --directory /path/to/directory --runInfoFile outputInfo.txt 
```

This will produce the text file outputInfo.txt which will contain a line for each subdirectory found in `/path/to/directory` that contains the word root in it. The first column is the run number and the second
column is the number of events.

## histogramScale2.py

WARNING: histogramScale2.py physically changes the histograms it runs on. So if it doesn't run correctly or something strange happens you may not be able to reverse it and your histograms
	will be "broken" so it is recommended to first make a copy of the histograms so that if something goes wrong you don't have to reproduce the histograms.

This script takes either two or three inputs depending on whether or not you want to use the AMI info for the number of events in the dataset. Again, it is not recommended to use AMI info for total events.

The first input is the directory that the histograms are located in that will be waited. The directory structure must be the same as that described in the script `getNumberOfEvents.py`. IE having subdirectories
which contain the root files, run number surrounded by periods, and contain the word root in them. And like the script `getNumberOfEvents.py` you must set `locationOfRunNumber` of the subdirectories near the
top of the script. 

The second input is the full path of the .txt file produced by the script `collectAMIInfo.py`.

The third input, which is only necessary (but recommended) if you set `useAMIForTotalEvents=false` near the top of the script `histogramScale2.py`, is the full of the .txt file produced by the script `getNumberOfEvents.py`

If you haven't done rcSetup or setup root yet in your current session you may need to do the command `localSetupROOT`.

Then from the directory that `histogramScale2.py` is located in run the command:
```
python histogramScale2.py --directory /some/path/to/directory --runInfoFile /full/path/XS_inputNames.txt --runTotalEventsFile /full/path/outputInfo.txt 
```

The script will probably take a while to run. It is going through each root file in the subdirectories and weighting all the histograms. It first copies all the histograms and adds the name "unweighted"
to the beginning of the name and then reweights the originals. 





	
