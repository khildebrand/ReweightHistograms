import os, argparse,glob, array
import signal
import sys
from ROOT import *
    
combinedHist = None
inFile = TFile.Open("combined_histograms.root")
for key in inFile.GetListOfKeys():
	if key.GetName().startswith('PtTruthAllAntiKt4TopoEM'):
		if not combinedHist:
			combinedHist = key.ReadObj()
			#print('using ' + key.GetName())
		else:
			#print(combinedHist)
			combinedHist.Add(key.ReadObj())

combinedHist.SetDirectory(0)
inFile.Close()

outFile = TFile("added_hists.root", "recreate")
combinedHist.Write()
outFile.Close()
