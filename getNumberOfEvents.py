import os, argparse,glob, array
import signal
import sys

bdebug=False
locationOfRunNumber=3
useCutflow=False
nameOfHistogram="EventCounter"
whichBin=1
treeName="CaloCalTopoClusters"
def debug(message):
    if bdebug:
        print message

def signal_handler(signal, frame):
        print('You pressed Ctrl+C!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)

parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#parser.add_argument("--directory", dest='directory', default="/share/t3data3/khildebr/RandomConeV204MC/", help="Path to the input directory")
parser.add_argument("--directory", dest='directory', default="/share/t3data3/khildebr/submitDirJESV65weighted/", help="Path to the input directory")
parser.add_argument("--runInfoFile", dest='runInfoFile', default="/home/khildebr/AMIRetrieve/EventCountsV65JES.txt", help="Path to the txt file to output too")

args = parser.parse_args()

from ROOT import *
## If output file already exists, remove it ##
if os.path.isfile( os.path.basename(args.runInfoFile) ):
  os.remove(os.path.basename(args.runInfoFile))
  
#check if directory exists
if not os.path.isdir(args.directory):
    print("%s directory does not exist"%(args.directory))
    sys.exit(0)
    
#check that there are subdirectories
if os.listdir(args.directory)==[]:
    print("%s directory is empty"%(args.directory))
    sys.exit(0)
    
#now have confirmed directory exists and is not empty
for subdirectory in os.listdir(args.directory):
    #only look at root subdirectories
    if "root" not in subdirectory or "driver.root" in subdirectory:
        continue
    debug("%s" %subdirectory)
    #get Run Number
    runNumber=subdirectory.split(".")[locationOfRunNumber]
    debug(runNumber)
    Events=0
    #print the list of files contained in subdirectory
    debug(os.listdir("%s/%s"%(args.directory,subdirectory)))
    fullPathOfSubdirectory="%s/%s"%(args.directory,subdirectory)
    debug(fullPathOfSubdirectory)
    for rootFile in os.listdir(fullPathOfSubdirectory):
        debug(rootFile)
        if ".root" not in rootFile:
            continue
        inFile = TFile.Open("%s/%s"%(fullPathOfSubdirectory,rootFile), "UPDATE")
        if useCutflow:
            cutFlow=[]
            for key in inFile.GetListOfKeys():
                #debug(key.GetName().split('"')[0])
                if nameOfHistogram==key.GetName().split("\"")[0]:
                    cutFlow = inFile.Get( key.GetName() )
            if cutFlow==[]:
                print "ERROR, no %s histogram found"%nameOfHistogram
                exit(1)   
            debug(cutFlow.GetBinContent(1))
            Events=Events +cutFlow.GetBinContent(1)
        else:
            tree = inFile.Get(treeName)
            Events=Events+tree.GetEntries()
        inFile.Close()
    with open(os.path.basename(args.runInfoFile), 'a') as g:
        print >> g, runNumber, Events
        
